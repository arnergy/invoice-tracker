// import Express from "express";

// const app = Express();
// const port = 8080;

const express = require('express');
const path = require('path');
const bp = require('body-parser')
const { fs } = require('file-system');

const app = express();
const port = process.env.PORT || 8080;

app.use(bp.json())
app.use(bp.urlencoded({ extended: true }))

app.get("/", (req, res) => {
    // res.send("Hello World");
    
    // user_value = req.headers["x-forwarded-user"];
    // console.log(user_value);

    // res.json({answer: user_value});
    // res.header("user_name", user_value)
    res.sendFile(path.join(__dirname, '/index.html'))
});

app.get("/user_name", (req, res) => {
    user_value = req.headers["x-forwarded-user"];
    console.log(user_value);

    res.json({answer: user_value});
})

app.post("/logger", (req, res) => {
    const out = req.body.data[1] + ',' + req.body.data[2] + ',' + req.body.data[3] + ',' + req.body.data[4] + ',' + req.body.data[5] + '\n'

    // if (fs.existsSync(path.join(__dirname, '/logger.csv'))) {
    //     fs.writeFileSync(path.join(__dirname, '/logger.csv'), out, {flag: 'a'});
    // }else {
    //     console.log('File not found')
    // }

    fs.appendFile(path.join(__dirname, '/logger.csv'), out, (err) => {
        if(err) console.log(err.message)
        else console.log(out)
    });
    

    // console.log(req.body.data[1], req.body.data[3], req.body.data[3], req.body.data[4], req.body.data[5])
    // console.log(out)
})

app.listen(port, () => console.log("Listenig on port" + port))