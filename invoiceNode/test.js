fetch('http://localhost/user_name')
    .then(res => res.json())
    .then(data => obj = data)
    .then(() => console.log(obj["answer"]))

const config = {
    data: [],
    dataSchema: { deviceId: null, customerName: null, expiryDate: null, uploaded: null, expiry: null },
    startRows: 10,
    startCols: 5,
    colHeaders: ['deviceId', 'Name of Customer', 'expiryDate', 'Uploaded to Solar Base (Yes/No)', 'Expiry Date Updated (Yes/No)'],
    height: 'auto',
    width: 'auto',
    columns: [
        { data: 'deviceId' },
        { data: 'customerName' },
        { data: 'expiryDate' },
        { data: 'uploaded' },
        { data: 'expiry' }
    ],
    minSpareRows: 1,
    height: 'auto',
    stretchH: 'all',
    minSpareRows: 1,
    autoWrapRow: true,
    contextMenu: true,
    licenseKey: 'non-commercial-and-evaluation'
};

document.getElementById("hooksList").style.visibility = "hidden";
const example1Events = document.getElementById("example1_events");
const hooksList = document.getElementById('hooksList');
const hooks = Handsontable.hooks.getRegistered();

hooks.forEach(function(hook) {
let checked = '';

if (hook === 'afterChange' || hook === 'afterCreateRow' || hook === 'afterRemoveRow' || hook === 'afterCreateCol' || hook === 'afterRemoveCol') {
    checked = 'checked';
}

hooksList.innerHTML += '<li><label><input type="checkbox" ' + checked + ' id="check_' + hook + '"> ' + hook + '</label></li>';
config[hook] = function() {
    log_events(hook, arguments);
}
});

const start = (new Date()).getTime();
let i = 0;
let timer;

function log_events(event, data) {
if (document.getElementById('check_' + event).checked) {
    const now = new Date().getTime();
    const diff = now - start;
    const dnow = new Date();
    let str;

    const vals = [i, '@' + dnow + " " + numbro(diff / 1000).format('0.000'), '[' + event + ']'];

    for (let d = 0; d < data.length; d++) {
    try {
        str = JSON.stringify(data[d]);
    } catch (e) {
        str = data[d].toString(); // JSON.stringify breaks on circular reference to a HTML node
    }

    if (str === void 0) {
        continue;
    }
    if (str.length > 20) {
        str = data[d].toString();
    }
    if (d < data.length - 1) {
        str += ',';
    }

    vals.push(str);
    }

    if (window.console) {
    console.log(i, '@' + dnow + " " + numbro(diff / 1000).format('0.000'), '[' + event + ']', data);
    }

    const div = document.createElement('div');
    const text = document.createTextNode(vals.join(' '));
    div.appendChild(text);
    example1Events.appendChild(div);
    clearTimeout(timer);
    timer = setTimeout(function() {
    example1Events.scrollTop = example1Events.scrollHeight;
    }, 10);

    i++;
}
}

const example1 = document.getElementById('example1');
const button = document.querySelector('#export-file');
const hot = new Handsontable(example1, config);

document.querySelector('#check_select_all').addEventListener('click', function() {
const state = this.checked;
const inputs = document.querySelectorAll('#hooksList input[type=checkbox]');
Array.prototype.forEach.call(inputs, function(input) {
    input.checked = state;
});
});
document.querySelector('#hooksList input[type=checkbox]').addEventListener('click', function() {
if (!this.checked) {
    document.getElementById('check_select_all').checked = false;
}
});

const exportPlugin = hot.getPlugin('exportFile');

// event to handle the export for csv
button.addEventListener('click', () => {
exportPlugin.downloadFile('csv', {
    bom: false,
    columnDelimiter: ',',
    columnHeaders: true,
    exportHiddenColumns: true,
    exportHiddenRows: true,
    fileExtension: 'csv',
    filename: 'Handsontable-CSV-file_[YYYY]-[MM]-[DD]',
    mimeType: 'text/csv',
    rowDelimiter: '\r\n',
    rowHeaders: true
});
});